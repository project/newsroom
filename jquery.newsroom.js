// Global killswitch: only run if we are in a supported browser
if (Drupal.jsEnabled) {
  $(document).ready(function(){

    $('a.newsroom-link').click(function(){
      var approveSaved = function (data) {
        var result = Drupal.parseJson(data);
        $('div#newsroom-approved-'+result['nid']).html(result['approved']);
      }
      $.get(this.href, null, approveSaved);
      return false;
    });

    $('a.newsroom-vote-link').click(function(){
      var voteSaved = function (data) {
        var result = Drupal.parseJson(data);
        $('span#newsroom-voted-'+result['nid']).html(result['voted']);
        $('span#newsroom-votes-'+result['nid']).html(result['vote_label']);
      }
      $.get(this.href, null, voteSaved);
      return false;
    });

  });
}
