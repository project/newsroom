Drupal Newsroom Module
Beta Release
Compatible with Drupal 5.x

Overview

The Drupal Newsroom provides an excellent way to add dynamic current events and news to your Drupal-powered Web site. It's also a great way to stimulate involvement and discussion in your online member community.

The Drupal Newsroom module provides a page for your Drupal-powered Web site with news stories based on specific topic(s) from NewsCloud.com or populated by your staff or site members. The newsroom module allows your members to browse headlines, read stories, vote, comment and post new articles.

The Drupal Newsroom allows organizations on different Web sites to collaborate, selecting stories for a topic-based newsroom, sharing the content and discussions from their site with other like-minded groups. 

Demonstration site:
http://newsroom.ourbridge.ca/

Learn more:
http://www.newscloud.org/index.php/Drupal_Newsroom_Module 

For questions, feedback, feature requests and bug reports, please email jeff@newscloud.com.

Programmed by Dave Tarc (dave.tarc@gmail.com)

